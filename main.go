package main

import (
	"fmt"
	"log"
	"net/http"
)

func main() {

	http.HandleFunc(
		"/hello",
		func(w http.ResponseWriter, r *http.Request) {
			fmt.Fprintf(w, "Hello!")
		},
	)

	http.HandleFunc(
		"/",
		func(w http.ResponseWriter, r *http.Request) {
			fmt.Fprintf(w, "Hello k8s go 2019!")
		},
	)

	log.Fatal(http.ListenAndServe(":8080", nil))
}
